using System;
using System.Drawing;

public class CustomRectangle
{
	// Private fields
    private Point _leftTopPosition;
    private int _width;
    private int _height;
	
	// Constructors
	public CustomRectangle(Point leftTopPosition, int width, int height)
	{
		_leftTopPosition = leftTopPosition;
        _width = width;
        _height = height;
	}
	
	// Properties
	public Point LeftTopPosition
    {
        get { return _leftTopPosition; }
        set { _leftTopPosition = value; }
    }

    public int Width
    {
        get { return _width; }
        set { _width = value; }
    }

    public int Height
    {
        get { return _height; }
        set { _height = value; }
    }
	
	// Static Methods
	public static CustomRectangle Merge(CustomRectangle r1, CustomRectangle r2)
	{
		int leftTopPositionX = Math.Min(r1._leftTopPosition.X, r2._leftTopPosition.X);
        int leftTopPositionY = Math.Max(r1._leftTopPosition.Y, r2._leftTopPosition.Y);

        int rightBottomPositionX = Math.Max(r1._leftTopPosition.X + r1._width, r2._leftTopPosition.X + r2._width);
        int rightBottomPositionY = Math.Min(r1._leftTopPosition.Y - r1._height, r2._leftTopPosition.Y - r2._height);

        Point resultLeftTopPosition = new Point(leftTopPositionX, leftTopPositionY);
        Point resultRightBottomPoint = new Point(rightBottomPositionX, rightBottomPositionY);

        int width = Math.Abs(resultLeftTopPosition.X - resultRightBottomPoint.X);
        int height = Math.Abs(resultLeftTopPosition.Y - resultRightBottomPoint.Y);

        return new CustomRectangle(resultLeftTopPosition, width, height);
	}
	
	public static CustomRectangle Intersect(CustomRectangle r1, CustomRectangle r2)
	{
		int r1Left = r1._leftTopPosition.X;
        int r2Left = r2._leftTopPosition.X;
        int r1Top = r1._leftTopPosition.Y;
        int r2Top = r2._leftTopPosition.Y;
        int r1Right = r1._leftTopPosition.X + r1._width;
        int r2Right = r2._leftTopPosition.X + r2._width;
        int r1Bottom = r1._leftTopPosition.Y + r1._height;
        int r2Bottom = r2._leftTopPosition.Y + r2._height;

        int left = Math.Max(r1Left, r2Left);
        int top = Math.Max(r1Top, r2Top);
        int right = Math.Min(r1Right, r2Right);
        int bottom = Math.Min(r1Bottom, r2Bottom);

        int positionX = Math.Max(r1._leftTopPosition.X, r2._leftTopPosition.X);
        int positionY = Math.Min(r1._leftTopPosition.Y, r2._leftTopPosition.Y);

        Point position = new Point(positionX, positionY);
        int width = (right > left) ? (right - left) : 0;
        int height = (bottom > top) ? (bottom - top) : 0;

        return new CustomRectangle(position, width, height);	
	}
	
	// Instance methods
	public override string ToString()
	{
		return string.Format("{0}, Width = {1}, Height = {2}", _leftTopPosition, _width, _height);
	}
}

public class Program
{
	public static void Main()
	{
		Console.WriteLine("{0}***RECTANGLES PROGRAM***{0}", Environment.NewLine);

        // Create two rectangles
        Console.WriteLine("---Create two rectangles---");
        CustomRectangle firstRectangle = new CustomRectangle(new Point(-4, 3), 4, 6);
        CustomRectangle secondRectangle = new CustomRectangle(new Point(0, -5), 3, 8);

        Console.WriteLine("firstRectangle: {0}", firstRectangle);
        Console.WriteLine("secondRectangle: {0}{1}", secondRectangle, Environment.NewLine);

        // Change coordinates, width, height of firstRectangle and secondRectangle
        Console.WriteLine("---Change leftTopPoint, width, height of firstRectangle and secondRectangle---");
        firstRectangle.LeftTopPosition = new Point(1, 6);
        firstRectangle.Width = 3;
        firstRectangle.Height = 5;
		
        secondRectangle.LeftTopPosition = new Point(2, 7);
        secondRectangle.Width = 4;
        secondRectangle.Height = 5;

        Console.WriteLine("firstRectangle: {0}", firstRectangle);
        Console.WriteLine("secondRectangle: {0}{1}", secondRectangle, Environment.NewLine);

        // Merge two rectangles
        Console.WriteLine("---Merge firstRectangle and secondRectangle---");
        Console.WriteLine("Merge result: {0}{1}", CustomRectangle.Merge(firstRectangle, secondRectangle), Environment.NewLine);

        // Intersect two rectangles
        Console.WriteLine("---Intersect firstRectangle and secondRectangle---");
        Console.WriteLine("Intersect Result: {0}{1}",
        CustomRectangle.Intersect(firstRectangle, secondRectangle), Environment.NewLine);
	}
}